package com.example.theinstuls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class thesky extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thesky);
    }

    public void Youtube(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/LlXOOG47Ykc"));
        startActivity(browserIntent);
    }
    public void Youtube1(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/8rZ5gE0LVew"));
        startActivity(browserIntent);
    }
    public void Youtube2(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/BpJMI-biOFI"));
        startActivity(browserIntent);
    }
}

